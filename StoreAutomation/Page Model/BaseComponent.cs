﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;

namespace StoreAutomation.Page_Model
{
    public class BaseComponent
    {
        public IWebDriver Driver;
        public Actions Actions;
        public WebDriverWait DriverWait;

        public BaseComponent(IWebDriver driver)
        {
            Driver = driver;
            DriverWait = new WebDriverWait(Driver, TimeSpan.FromSeconds(15));
            Actions = new Actions(Driver);
        }

        public IWebElement SearchField => Driver.FindElement(By.Name("search_query"));

        public IWebElement SearchButton => Driver.FindElement(By.Name("submit_search"));

        public IWebElement Close => Driver.FindElement(By.ClassName("cross"));

        public IList<IWebElement> ProductList => Driver.FindElements(By.XPath("//ul[@id='homefeatured']//li"));

        public IWebElement ViewCart => Driver.FindElement(By.XPath("//a[@title='View my shopping cart']"));

        public IWebElement SeleniumFrameworkLink => Driver.FindElement(By.LinkText("Selenium Framework"));

        public string CurrentURL => Driver.Url;

        public IWebElement ProceedToCheckoutButton => Driver.FindElement(By.XPath("//a[@title='Proceed to checkout']"));

        public IWebElement AddToCart(int dataIdProduct)
        {
            return Driver.FindElement(By.XPath($"//a[@data-id-product='{dataIdProduct}']"));
        }

        public void HoverEffect(IWebElement element)
        {
            Actions.MoveToElement(element).Build().Perform();
        }
    }
}
