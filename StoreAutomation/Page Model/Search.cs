﻿using OpenQA.Selenium;

namespace StoreAutomation.Page_Model
{
    public class Search : BaseComponent
    {
        public Search(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement ProductLink => Driver.FindElement(By.XPath("//a[@title='Faded Short Sleeve T-shirts' and @class='product-name']"));
    }
}
