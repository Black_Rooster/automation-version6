﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace StoreAutomation.Helper_Class
{
    public static class TotalHelper
    {
        public static string GetProductTotalPrice(IList<IWebElement> ProductPriceList)
        {
            double total = 0;

            foreach (var price in ProductPriceList)
            {
                total += double.Parse(price.Text.Remove(0, 1), CultureInfo.InvariantCulture);
            }

            var temp = $"${Math.Round(total, 2)}".Replace(",", ".");

            return temp;
        }
    }
}
