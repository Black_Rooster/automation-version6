﻿using NUnit.Framework;

namespace StoreAutomation.Test
{
    [TestFixture]
    public class RemoveFromCart : Base
    {
        [Test]
        public void GivenProductName_WhenRemovingProductInTheCart_ThenRemoveProduct()
        {
            StoreSetup.BaseComponentPage.SearchField.SendKeys("Faded Short Sleeve T-shirts");
            StoreSetup.BaseComponentPage.SearchButton.Click();
            StoreSetup.SearchPage.ProductLink.Click();
            StoreSetup.ProductDetailPage.AddToCartButton.Click();
            StoreSetup.BaseComponentPage.ProceedToCheckoutButton.Click();
            StoreSetup.CartPage.RemoveProduct.Click();
            StoreSetup.CartPage.WaitForProductCounterUpdate();

            Assert.AreEqual("(empty)", StoreSetup.CartPage.ProductCounter.GetAttribute("textContent"));
        }
    }
}
